library(dplyr)
MechaCar <- read.csv(file = 'MechaCar_mpg.csv',check.names = F, stringsAsFactors = F)

#Linear Regression time
lm(mpg ~ vehicle_length + vehicle_weight + spoiler_angle + ground_clearance + AWD, data= MechaCar)

summary(lm(mpg ~ vehicle_length + vehicle_weight + spoiler_angle + ground_clearance + AWD, data= MechaCar)
)


#Technical Analysis
SuspensionCoil <-  read.csv(file= 'Suspension_Coil.csv', check.names= F, stringsAsFactors = F )

total_summary <- SuspensionCoil %>% summarize(Mean = mean(PSI), Median = median(PSI), Variance = var(PSI), SD = sd(PSI))

lot_summary <- SuspensionCoil %>% group_by(Manufacturing_Lot)%>% summarize(Mean = mean(PSI), Median = median(PSI), Variance = var(PSI), SD = sd(PSI))

#T test town

t.test(SuspensionCoil$PSI, mu= mean(SuspensionCoil$PSI))

t.test(subset(SuspensionCoil, SuspensionCoil$Manufacturing_Lot=='Lot1')$PSI, mu= mean(SuspensionCoil$PSI))

t.test(subset(SuspensionCoil, SuspensionCoil$Manufacturing_Lot=='Lot2')$PSI, mu= mean(SuspensionCoil$PSI))

t.test(subset(SuspensionCoil, SuspensionCoil$Manufacturing_Lot=='Lot3')$PSI, mu= mean(SuspensionCoil$PSI)) 



