## Challenge 16

# Linear Regression 
    1. Vehicle Weight, Spoiler Angle, and if the vehicle has All Wheel drive all effect the Miles per Gallon of the MechaCar. with the spoiler angle having the largest impact. 
    2. The P value is 5.35e-11 which is much less than the significance value of .05 so we can safely assume that the slope of the model is not 0. 
    3. This model does somewhat effectively predict the MPG of the vehicle which you can see with the R-Squared value of 0.7149.


# Summary of Suspension Coils
    1. The variance of all the coils is under the manufacturer's design at approximately 62.29 PSI however this is mostly because the variance in lot 3 is wildly out of wack with a variance of 170.28 PSI while lots 1 and 2 are well within limits with variance levels of .98 and 7.47 respectively. 

# T Tests on Suspension Coils
    1. It appears as if Lot 1 and 2 are both statistically similar with p values of 1.568e-11 and 0.0005911 respectively, however lot 3 is not similar with a p value of 0.1589.

# Study Design: MechaCar vs Competition
    1. We should also study the fuel efficiency of the MechaCar in comparison to its Competition. 
    2. The null hypothesis is that the MC has similar fuel efficiency to it's competition, Alternately the MC has better Fuel efficiency.
    3. To do this we can do a two sample T test of the Miles per Gallon of both the MC and its best competition
